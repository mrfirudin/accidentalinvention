---
title: Agile Data Science 2.0    
subtitle: A One Book A Day Deep Dive
date: 2021-03-21
tags: ["intelligence", "genuine", "OBADDD", "TBD0"]
---


Today's One Book A Day Deep Dive is relatively new report from [O'Reilly's Graph Analytics Topic](https://learning.oreilly.com/topics/graph-analytics/), put together by Sean Martin, Ben Szekely, and Dean Allemang explaining what is different about graph data and an explict representation of knowledge visually and demonstrating an example that could morph into an enterprise-scale data fabric combining these two things in a knowledge graph. Published in March 2021 by O'Reilly, [The Rise of the Knowledge Graph](https://learning.oreilly.com/library/view/the-rise-of/9781098100407/) is a short, high level industry report that delves into the evolution of databases, data integration, data analysis and tells people enough about *how we got here* that they can knowledgably begin to explore a lot further and deeper if their interest takes them there. This report is an extremely useful little summary ... a great jumping off point.


[Emergence of the Knowledge Graph](https://learning.oreilly.com/library/view/the-rise-of/9781098100407/ch01.html#emergence_of_the_knowledge_graph) ... the history goes back to the early days of AI research, where a number of systems were developed to allow machines to understand the meaning of data, collectively referred to as semantic systems. The moniker knowledge graph itself, emerged after 2012, used to describe any distributed graph data and metadata system, based on the same principles of linking distributed, meaningful data entities and appropriate services in a graph structure.

[Data and Graphs: A Quick Introduction](https://learning.oreilly.com/library/view/the-rise-of/9781098100407/ch01.html#data_and_graphs_a_quick_introduction)

[The Knowledge Layer](https://learning.oreilly.com/library/view/the-rise-of/9781098100407/ch01.html#the_knowledge_layer)

[Managing Vocabularies in an Enterprise](https://learning.oreilly.com/library/view/the-rise-of/9781098100407/ch01.html#managing_vocabularies_in_an_enterprise)

[Representing Data and Knowledge in Graphs: The Knowledge Graph](https://learning.oreilly.com/library/view/the-rise-of/9781098100407/ch01.html#representing_data_and_knowledge_in_graphs_the_kno)

[Beyond Knowledge Graphs: An Integrated Data Enterprise](https://learning.oreilly.com/library/view/the-rise-of/9781098100407/ch01.html#beyond_knowledge_graphs_an_integrated_data_enterp)
