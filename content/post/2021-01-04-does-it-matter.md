---
title: Does It Matter
subtitle: Why SCOPE management is the HARDEST part of HARD projects
date: 2021-01-04
tags: ["intelligence", "genuine", "TBD0"]
---


JFK injected a massive dose of optimism into an American country that was in a complete funk coming out of 1960s.

We SHOULD do HARD things ... in the way that going to the Moon by the end of the decade was a HARD thing on JFK's inauguration ... the country needed the kind of jumpstart that one gets from going for really, really long run ... to the Moon. And back!

It wasn't Mars bullshit.  It was a HARD goal to jumpstart and re-invigorate America ... to Make America Great Again!

But repeating it is STUPID.  Why does dropping into a gravity well and then climbing back out of it again matter?  Because it's HARD?

Think again Einstein!

We are surrounding by HARD things ... developing a Type V Kardasev multiverse would be a HARD thing that we SHOULD do ... ASAP ... because it does matter ... but before we do that, we had better think about SCOPE MANAGEMENT.