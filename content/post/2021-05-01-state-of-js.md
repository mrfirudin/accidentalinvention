---
title: State of JS
subtitle: AWESOME lists of lists
date: 2021-01-01
tags: ["awesome", "TBD0"]
---


In something *expansive* like the Javascript universe, it helps to have a [set of Best Of lists](https://bestofjs.org/projects) to know [who the hall of fame players are](https://bestofjs.org/hall-of-fame) OR [when the big projects started or got traction](https://bestofjs.org/timeline) ... especially in something like the [GraphQL space](https://bestofjs.org/projects?tags=graphql) ... some things are kind of obvious, for example, [TypeScript](https://2020.stateofjs.com/en-US/technologies/javascript-flavors/) is the only flavor of Javascript worth using.  While the [datalayer space](https://2020.stateofjs.com/en-US/technologies/datalayer/) it's now clear that GraphQL and its related data schema technologies are here to stay.  The [back-end space](https://2020.stateofjs.com/en-US/technologies/back-end-frameworks/) is still very fragmented, but Express stands out as the one dominant tool and kind of the second anchor of the MEAN / MERN / MEVN acronym space ... while Next.js maintains its high satisfaction ratio.