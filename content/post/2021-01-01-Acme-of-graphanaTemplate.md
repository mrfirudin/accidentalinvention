---
title: The Grafana template
subtitle: Acme MEANS getting your company name first in the directory; it's meaning as a word is immaterial
date: 2021-01-01
tags: ["intelligence", "TBD1"]
---


Stop being a sophisticated, know-esoterica-all IDIOT ... start being a little more shrewd, a LOT more practical.

If you want to do that you are going to need to automate and continuously improve your listening ... that means learning about stuff like Grafana ... should you understand everything about Grafana -- not necessarily, you should understand what it's for, what tools like Grafana do ... listen, interact, play, develop a genuine shared intelligence. 

After playing with dashboards, you might become really curious about [the robust, free tier of observability](https://grafana.com/?pg=play&plcmt=home) from [Grafana](https://grafana.com/grafana/) and the open source [Grafana project on Github](https://github.com/grafana/grafana) ... you might even want to learn what is necessary to imitate something like the [Grafana playground](https://play.grafana.org/d/000000012/grafana-play-home?search=open&orgId=1).