---
title: Keep Up If You Want To Be Competent
subtitle: Nobody can be COMPETENT for you.
date: 2021-01-02
tags: ["intelligence", "genuine", "competent", "TBD0"]
---


In order to be a [**competent** polymath](https://en.wikipedia.org/wiki/Competent_man), you will need to stay current and keep up ... that means being a **practical** polymath OR keeping up with the current world. 

Genuine comptetence not about the old, esoteric or romantic requirements that made sense to some English major ... not likely to be about conning a ship or pitching manure or planning an invasion. The general principle of [polymathic **competence**](https://en.wikipedia.org/wiki/Competent_man) is still important -- but it needs to much more practical and down to Earth. You will need to exercise your skillset in some sort of fashion where people [who know what they are doing well enough to evaluate you] can see whether you are for real.

Being competent does not imply mastery ... mastery involves extremely serious, long-term focus, eg 10,000 GOOD reps, but keeping up does depends upon YOU and YOUR efforts to be actively engaged in keeping up.  It's about being able to DO it, not necessarily being the best at doing it. 

Keeping up is not something on Wikipedia or something you can Google or look for a YouTube video ... and even more certainly, keeping up is definitely not about anything that will be broadcast or printed in any sort of traditional media for passive consumption by the programmed masses ... and, it's not some documentary or ever going come out on *some video you really need to see.*

If you want to be a [**competent** modern human-being](https://en.wikipedia.org/wiki/Competent_man), your intelligence-gathering operation cannot be trusted to someone else ... keeping up is going involve a lot of experimentation and learning stuff the hard way ... but that involves a wide variety of educational experiences ... wrestling and lifting and training extreme sports, but also coding and using automation or employing machine learning and artificial intelligence as well as growing one's own food and a variety of pursuits which are about understanding the industrial and logistic systems or economics and trading that life depends upon ... interests of the [anti-specialist](https://en.wikipedia.org/wiki/Competent_man) are intensely practical.

 There's no news organization that is a substitute for a well-curated social media profile, a personally well-curated clone of [AWESOME](https://github.com/sindresorhus/awesome#readme) lists [of repositories] and and some sort of content management system reviewing, tagging, categorizing what you have learned from the universe of pre-print arXivs, music, job posting ... your awareness of what is out there or developing your own indicators of competence is not something that someone else can do, either -- it is ALL entirely up to YOU.