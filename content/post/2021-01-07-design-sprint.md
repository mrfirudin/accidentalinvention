---
title: Design Sprint    
subtitle: Find and answer critical business questions
date: 2021-01-08
tags: ["intelligence", "genuine", "TBD0"]
---


A [Design Sprint](https://www.gv.com/sprint/), is a 5-day process used to answer critical business questions through design, prototyping and testing ideas with customers ... the point is to reduce cycle time in when coming up with a LEAN solution that is needed, rather than features that seem neat. 

In each of the five days, a design sprinter is expected to accomplish several items:

- Day 0: preparatory exercises, do the homework, show up prepared OR don't bother
- Day 1: map out the customer value stream map and impediments to achieving goals
- Day 2: diverge with ideas and list competing solutions
- Day 3: converge on decisions and establish a testable hypothesis
- Day 4: craft a minimum prototype
- Day 5: get feedback on the prototype 
