---
title: Practical survivalism as a sport
subtitle: It's like wrestling in the Olympics ... except you're more likely to die.
date: 2021-03-05
tags: ["intelligence", "genuine", "TBD0"]
---


Somewhat like Mudders or Ironman events, we have been ruminating on this Winter endurance event, which we have given the working name: "Odin's Wrath"

It's a sport that involves living without utilities ... for long periods of time, like a decade or three ... and then, in all likelihood, going back to one's Creator and dying an excellent death, outdoors. 

It's a sport for old men ... really TOUGH old men ... because young pups are not really tough and *resourceful* enough to do it ... they cannot be, because they have not yet really come to terms with the *necessity of* dying.

Did we mention the part about it's a test of *resourcefulness* ... that's why AncientGuy.Fitness is MOST DEFINITELY **ABSOLUTELY NOT** about some pathetic, hokey bullshit that involves having the body of some candyass fortysomething -- AncientGuy.Fitness is about training for Odin's Wrath. The world is full of that kind of WEAK, clinging hokum!

Of course, the POINT of "Odin's Wrath" is not dying outdoors, ie that's a benefit for those who recognize the necessity of the finiteness of physical Life... the POINT of training for the thing is about building and sharing the knowledge of resourcefulness.