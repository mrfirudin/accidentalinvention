---
title: Microfluidic Flow Cytometry
subtitle: Horton Hears A Who
date: 2021-03-10
tags: ["intelligence", "genuine", "TBD0"]
---


The Dr. Suess classic helps some of us think/talk about SCALE ... or maybe it helps us to understand why there's so much room at the bottom and that stuff has its own "story," ie you don't need to go thousands of miles away to explore when you have a 2500X tissue micropropagation microscope. You are not going to be able to have "conversations" with those materials, but you can start to understand what the sensors and I/O are going to need to look like, in very vague, general *30,000 ft* terms. 

The micro-scale things that visible with 2500X microscope are almost good enough to almost sorta see cells, but at that level we cannot really look inside that cell, ie we cannot see DNA. Below at serious nano-scale and really "seeing molecules" is ANOTHER 1000X further down but we're probably no longer "looking" with visible light optics. But a good microscope is still a giant leap forward, because that microscope is definitey not high-precision jewelry scale which is human visible but probably benefits from using a 2.5X magnifying glass.  

Yeah, developing a botanical quantum computer is pretty much [just a LITTLE plumbing](https://www.ufluidix.com/microfluidics-research-reviews/microfluidic-flow-cytometry-principles-and-commercial-review/) ... 

Ok, it's HARD ... because there as several problems to solve ... before we can even get to the point where we are just LOOKING at the issue ... never mind about the 1% inspiration and 99% perspiration parts that'll follow ... AFTER we can *see* what we're doing.

