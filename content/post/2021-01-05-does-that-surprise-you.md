---
title: Does That Surprise You?
subtitle: If it doesn't, it is not information, it is hypnosis.
date: 2021-01-05
tags: ["intelligence", "genuine", "TBD0"]
---


Look back at Claud Shannon's work ... go ahead ... we'll wait.

The ONE thing that we should talk about it would be what exactly information is. 

If something surprises you, even modestly, that's information.  If it merely comforts / soothes / entertains you or re-affirms what you already know -- that's hypnosis.