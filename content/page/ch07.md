---
title: Chapter 7
subtitle: Invent your own innovation workflow and toolchain
comments: false
---

You're free to come up with whatever works, but you probably are going to want a workflow and toolchain (eg, Gitlab, Hugo, Joplin, Visual Code, GitKraken), not just time management which is Chapter 8, that allows to habitually, methodically, continuously improvise, improve, innovate and invent ... if you need a break from your workflow, you're doing it wrong -- although your workflow should vary and include LOTS of exercise.

Whatever you do, you really need to understand the importance of just staying at it. Understand compounding of small evolutionary changes as you apply the theory and principles behind methods like set-based concurrent engineering, ishikawa root cause analysis or TRIZ/ARIZ.