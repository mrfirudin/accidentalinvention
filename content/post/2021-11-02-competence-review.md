---
title: Keep Up If You Want To Be Competent
subtitle: Nobody can be COMPETENT for you.
date: 2021-11-02
tags: ["intelligence", "genuine", "competent"]
---


In order to be a [**competent** polymath](https://en.wikipedia.org/wiki/Competent_man), you will need to stay current and keep up ... being a **practical** polymath involves a periodic review of one's competency ... there's no *competent news* organization -- it's up to YOU ... this monthly review is no substitute for a well-curated social media profile, a personally well-curated clone of [AWESOME](https://github.com/sindresorhus/awesome#readme) lists [of repositories] and and some sort of content management system reviewing, tagging, categorizing what you have learned from the universe of pre-print arXivs, music, job posting ... your awareness of what is out there or developing your own indicators of competence is not something that someone else can do, either -- it is ALL entirely up to YOU.