---
title: What's up with the XIV tags
subtitle: arXiv, bioRxiv, medRxiv, chemRxiv, 
date: 2021-01-02
tags: ["intelligence", "genuine", "XIV0", "TBD0"]
---


[arXiv](https://arxiv.org/) is an open-access archive of PRE-PRINT, not yet peer-reviewed articles in the fields of physics, mathematics, computer science, quantitative biology, quantitative finance, statistics, electrical engineering and systems science, and economics. [bioRxiv](https://www.biorxiv.org/), [medRxiv](https://www.medrxiv.org/), [chemRxiv](https://www.medrxiv.org/), [engRxiv](https://www.engrxiv.org/) are the PRE-PRINT servers for Biology, Medicine, Chemistry and Engineering, respectively.

We try to scan through articles in our areas of interest ... MAYBE write a post ... PROBABLY never come back to it, but maybe we do ... so if we do we tag a post with XIV-prefix ... the last digit is an indicator or how many times we have come back to book 

XIV0 ... just picked up, initial flip-thru reaction, not really edited or revised at all yet

XIV1 ... has had only one revision ... which is a big deal ... even if we are just fixing gramamar or spelling

XIV2 ... has had second revision, MAYBE we might keep this idea to generate more ideas

XIV3 ... has had third revision, HOW could we fix this into something that might be worthwhile

XIV4 ... has had fourth revision, let's try to think more seriously about tightening this idea up

XIV5 ... has had fifth revision, after it sits for another month or so, maybe we then can revise it into #RoughDraft status.