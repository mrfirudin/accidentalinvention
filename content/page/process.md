---
title: Process
subtitle: ENGRco constantly researches, tests, uses, becomes, is its own open source product.
comments: false
---

In order to learn and teach, we write, revise, refactor, re-write open source collaborative labs ... the person who learns the most in any classroom is the one doing the teaching, so we attempt to teach people who are interested in learning to clone our repositories, fork our code, develop their own open source collaborative labs. The world cannot afford to invest any more in proprietary, closed-source educational institutions ... the modern educational institution must exist to train the people learning to be able to train [without a teacher or babysitter] other people. The education of nonlinear thinkers cannot occur in fixed, DEAD institution.

[Dogfooding](https://en.wikipedia.org/wiki/Eating_your_own_dog_food) is the practice of an organization using and improving its own OPEN SOURCE tools. ENGRco is fundamentally about Systems Engineering, Data Engineering, ML Engineering ... we use, improve and dogfood our Bodies of Knowledge in order to advance open sourced Bodies of Knowledge ... the evolution of this development can be seen in *traditional* [Systems Engineering](https://en.wikipedia.org/wiki/Systems_engineering) and the additional disciplines Applied [Complex Systems](https://en.wikipedia.org/wiki/Complex_system) -- and, of course,we might be finished with proprietary technologies and the antiquated thinking inherent in those technologies, but *we are not done dogfooding!*
